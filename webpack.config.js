const CopyWebpackPlugin = require( 'copy-webpack-plugin' );

module.exports = {
	mode: 'production',
	optimization: {
		minimize: false
	},
	entry: {
		popup: './src/ui/popup.jsx',
		background: './src/background/background.js'
	},
	output: {
		filename: '[name].js',
		path: __dirname + '/dist',
		publicPath: './dist'
		
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: [[
							'@babel/preset-env',
							{
								targets: {
									esmodules: true,
								},
							},
						],],
						plugins: [
							["@babel/plugin-transform-react-jsx", {"pragma": "h"}]
						]
					}
				}
			},
			{
				test: /\.scss$/i,
				use: [
					'style-loader',
					'css-loader',
					'resolve-url-loader',
					'sass-loader',
				],
			},
		]
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	devtool: 'inline-source-map',
	// devServer: {
	// 	contentBase: path.join( __dirname, 'public' ),
	// 	watchContentBase: true,
	// },
	plugins: [
		new CopyWebpackPlugin( { patterns: [
			{from: 'manifest.json'},
			{from: 'cancel-icon.png'},
			{from: 'src/ui/popup.html'},
			{from: 'src/background/components', to: 'components'},
			{from: 'src/background/background.js'},
			{from: 'src/background/background.html'},
		]} )
	]
};
