import {h, Component} from 'preact';
import TrackerList from './components/tracker-list'
import WhitelistOptions from './components/whitelist-options';

class App extends Component {

	constructor( props ) {
		super( props )
		console.log( 'UI initializing' )
		this.state = {
			currentTab: {
				id: null,
				hostname: '',
				trackersFound: {},
				whitelisted: false
			}
		}
	}

	componentDidMount () {
		chrome.runtime.onMessage.addListener( ( tabData ) => {
			this.setState( {currentTab: tabData} )
		} );

		// initial request
		chrome.runtime.sendMessage( {type: 'requestCurrentTab'} )

		// Get the updated list regularly while popup is open
		this.intervalId = setInterval( () => {
			chrome.runtime.sendMessage( {type: 'requestCurrentTab'} )
		}, 1000 );
	}

	componentWillUnmount () {
		// Not really needed because page is destroyed upon close but just in case
		clearInterval( this.intervalId )
	}

	toggleWhitelist ( e ) {
		e.preventDefault()
		let payload = {type: 'toggleWhitelist', payload: {id: this.state.currentTab.id, value: e.target.checked}}
		chrome.runtime.sendMessage( payload )
		chrome.tabs.reload( this.state.currentTab.id )
		window.close()
	}

	render () {
		return (
			<div id='app-root' style='text-align: center;'>
				<header>
					<h1 id='brand'>
						<span>Tracker Blocker </span>
						<span>📵</span>
					</h1>
				</header>
				<TrackerList tab={this.state.currentTab} />
				<WhitelistOptions whitelisted={this.state.currentTab.whitelisted} toggleWhitelist={this.toggleWhitelist.bind( this )} />
			</div>
		)
	}
}

export default App;