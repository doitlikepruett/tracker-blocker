import {h, Component} from 'preact';

function buildList ( list ) {
	let output = ''
	for (let key in list ){
		output += `${key} - (${list[key].toUpperCase()}) \n`
	}
	return output
}

const TrackerList = ( {tab} ) => {
	let listLen = Object.keys( tab.trackersFound ).length
	return (
		<div>
			{
				listLen ?
					<h3 style='color: darkred;'>Trackers Found: {listLen} </h3> :
				<h3 style='color: darkgreen;'>No Trackers Found!</h3>
			}
			<div>{tab.hostname}:</div>
			<textarea id="tracker-list" cols="40" rows="10" readOnly>
				{buildList( tab.trackersFound )}
			</textarea>
		</div>
	)

}

export default TrackerList