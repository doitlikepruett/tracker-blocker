import {h, Component} from 'preact';

const WhitelistOptions = ( {toggleWhitelist, whitelisted} ) => {
	return (
		<p>
			<div>
				<input type="checkbox" checked={whitelisted} onChange={toggleWhitelist} />
				<label title='Disable tracker blocking while visiting this site'>{
					whitelisted ? 'Remove from whitelist' : 'Whitelist this site'
				}</label>
			</div>
		</p>
	)
}

export default WhitelistOptions
