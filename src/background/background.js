import Interceptor from './components/interceptor'
import tabManager from './components/tab-manager';
import whitelistManager from './components/whitelist-manager'
import trackerlistManager from './components/trackerlist-manager';

function App () {
	console.log( 'App initializing' );

	async function init () {
		console.log( 'App init executing' );
		await whitelistManager.init()
		await trackerlistManager.init()
		await tabManager.initTabs()
		let interceptor = Interceptor( trackerlistManager.getTrackerList() )
		initListeners( {interceptor} )

		// setInterval( () => {
		// 	console.log( 'All Tabs', tabManager.getTabs() )
		// }, 4000 );
		// console.log( 'App init complete' )
	}
	
	function initListeners ( {interceptor} ) {
		console.log( 'App initializing listeners' );
		// requests
		chrome.webRequest.onBeforeRequest.addListener(
			interceptor.handler,
			{
				urls: ['<all_urls>'],
				types: ["image", "main_frame", "object", "other", "script", "stylesheet", "sub_frame", "xmlhttprequest"]
			},
			['blocking']
		)

		// tabs
		chrome.tabs.onUpdated.addListener( ( tabId, changeInfo, tab ) => {
				tabManager.createOrUpdateTab( tab )
		} )

		chrome.tabs.onRemoved.addListener( ( tabId, removeInfo ) => {
			tabManager.deleteTab( tabId )
		} )

		chrome.tabs.onCreated.addListener( tab => {
			if ( tab.status === 'complete' ) {
				tabManager.createOrUpdateTab( tab )
			}
		} )
		
		// messages
		chrome.runtime.onMessage.addListener( ( {type, payload} ) => {
			if ( type === 'requestCurrentTab' ) {
				requestCurrentTabHandler()
			} else if ( type === 'toggleWhitelist' ) {
				tabManager.toggleWhitelist( {id: payload.id, whitelisted: payload.value} )
			}
		} );
	}

	function requestCurrentTabHandler () {
		chrome.tabs.query( {'active': true, 'lastFocusedWindow': true}, function( tabData ) {
			if ( tabData.length ) {
				let internalTab = tabManager.getTab( tabData[0].id ) || {}
				let response = {
					id: internalTab.id,
					trackersFound: internalTab.trackersFound,
					hostname: internalTab.hostname,
					whitelisted: internalTab.whitelisted
				}
				chrome.runtime.sendMessage( response )
			}
		} );
	}


	init()
		.catch( err => {
			console.error( 'Error initializing: ', err )
		} )
}

App()

