'use strict'

import Tab from './tab.js'
import whitelistManager from './whitelist-manager'

function TabManager () {

	const _tabList = {}

	function deleteTab ( tabId ) {
		let tab = _tabList[tabId]
		tab && delete _tabList[tabId]
		return tab
	}

	async function initTabs () {
		return new Promise( ( resolve ) => {
			chrome.tabs.query( {currentWindow: true, status: 'complete'}, function( currTabs ) { // TODO: change currentWindow to true for production
				currTabs.forEach( tabData => {
					if ( tabData.url ) {
						createOrUpdateTab( tabData )
					} // else tab doesn't have a url yet
				} )
				// console.log( 'initTabs all tabs: ', JSON.stringify(getTabs()) )
				resolve()
			} );
		} )
	}

	function createOrUpdateTab ( tabData ) {
		if ( tabData.tabId ) { // to normalize when getting tabId from requestData
			tabData.id = tabData.tabId
		}

		if ( _tabList[tabData.id] ) {
			_tabList[tabData.id].update( tabData )
		} else {
			_tabList[tabData.id] = Tab( tabData )
		}
	}

	function getTab ( id ) {
		return _tabList[id]
	}

	function getTabs () {
		return _tabList
	}

	function toggleWhitelist ( {id, whitelisted} ) {
		let tab = getTab( id )
		tab.whitelisted = whitelisted
		whitelisted ? whitelistManager.addToWhitelist( tab.hostname ) : whitelistManager.removeFromWhitelist( tab.hostname )
		deleteTab( id )
		createOrUpdateTab( tab ) // hacky, but easier than refactoring update atm 
	}

	return {
		createOrUpdateTab,
		deleteTab,
		getTab,
		getTabs,
		initTabs,
		toggleWhitelist,
	}
}

export default TabManager()