import {parse} from 'tldts'
import whitelistManager from './whitelist-manager'

function Tab ( tabData ) {
	// console.log( 'tabData :>> ', tabData );
	let context = {
		id: tabData.id,
		status: tabData.status,
		url: tabData.url,
		trackersFound: {},
		hostname: parse( tabData.url ).hostname,
		whitelisted: isOnWhitelist( tabData.url ),
		update,
		recordTracker,
	}

	function isOnWhitelist ( url ) {
		let {hostname} = parse( url )
		let wl = whitelistManager.getWhitelist()
		let found = wl[hostname]
		return !!found
	}

	function update ( {id, url, status} ) {
		// console.log('update tab :>> ', id, url, status);
		id && ( context.id = id )
		status && ( context.status = status )
		url && ( context.url = url )
		let site = url ? parse( url ).hostname : context.hostname
		if ( site !== context.hostname ) {
			// console.log( 'Tab site update, Resetting tab', site, context.hostname );
			context.trackersFound = {}
			context.hostname = site
			context.whitelisted = isOnWhitelist(url)
		}
		return context
	}

	function recordTracker ( {domain, action} ) {
		context.trackersFound[domain] = action
	}

	return context
}

export default Tab