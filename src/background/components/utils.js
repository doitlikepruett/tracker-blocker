export const getFromStorage = function( key ) {
	return new Promise( ( res, rej ) => {
		try {
			chrome.storage.local.get( key, ( data ) => {
				res( data[key] )
			} )
		} catch ( err ) {
			rej( err )
		}
	} )
}

export const setOnStorage = function( dataToSet ) {
	return new Promise( ( res, rej ) => {
		try {
			chrome.storage.local.set( dataToSet, ( data ) => {
				res( data )
			} )
		} catch ( err ) {
			rej( err )
		}
	} )
}
