import {parse} from 'tldts'
import tabManager from './tab-manager';


function Interceptor ( trackerList ) {

	function handler ( requestData ) {
		// console.log( 'interceptor requestData:>> ', requestData );
		// console.log('tabManager :>> ', tabManager);

		let {tabId} = requestData

		if ( tabId === -1 ) {
			return;
		}

		let tab = tabManager.getTab( tabId )
		// console.log('tab :>> ', tab);
		if ( tab ) {
			let {domain} = parse( requestData.url )
			let tracker = trackerList.trackers[domain] || null

			if ( tracker ) {
				if ( tracker.default !== 'block' || tab.whitelisted ) {
					tab.recordTracker( {domain, action: 'ignored'} )
				} else {
					tab.recordTracker( {domain, action: 'blocked'} )
					// console.log( 'FOUND A TRACKER :>> ', tracker );
					// TODO: Handle rules?
					return {cancel: true} // cancel the request
				}
			}
		}

	}

	return {
		handler
	}
}

export default Interceptor


