'use strict'
import {getFromStorage, setOnStorage} from './utils'


function WhitelistManager () {

	let _whitelist

	function addToWhitelist ( site ) {
		_whitelist[site] = true
		setOnStorage( {whitelist: _whitelist} ) //sync
	}

	function getWhitelist () {
		return _whitelist
	}

	function removeFromWhitelist ( site ) {
		delete _whitelist[site]
		setOnStorage( {whitelist: _whitelist} ) //sync
	}

	async function init () {
		console.log( 'WhitelistManager initializing' );
		let list = await getFromStorage( ['whitelist'] )
		if ( !list ) { // first time
			list = {}
			setOnStorage( {whitelist: list} )
		}
		_whitelist = list
		return list
	}

	return {
		addToWhitelist,
		getWhitelist,
		init,
		removeFromWhitelist,
	}
}


export default WhitelistManager()
