'use strict'

import {getFromStorage, setOnStorage} from './utils'

function TrackerlistManager () {
	let _trackerList

	async function init () {
		console.log( 'TrackerlistManager initializing!' );
		_trackerList = await getFromStorage( ['trackerList'] )

		if ( _trackerList?.trackers && ( Date.now() - _trackerList?.fetchDate ) < 172800000 ) { // refresh the list every two days. 
			console.log( 'Tracker list not needed to be fetched' )
		} else {
			// TODO: How to handle a failed call, retry or just use old list?
			let rawList = await fetch( 'https://staticcdn.duckduckgo.com/trackerblocking/v2/tds.json' )
			let parsedList = await rawList.json()
			parsedList.fetchDate = Date.now()
			_trackerList = parsedList
			console.log( 'Fetched the tracker list', parsedList );
			setOnStorage( {trackerList: parsedList} )
		}
	}

	function getTrackerList () {
		return _trackerList
	}

	return {
		getTrackerList,
		init
	}
}

export default TrackerlistManager()
