import Interceptor from '../../components/interceptor'
import tabManager from '../../components/tab-manager'
jest.mock( '../../components/tab-manager' )

const mockTrackerList = {
	trackers:
	{
		'blackhat.com': {default: 'block'},
		'greyhat.com': {default: 'block'},
		'whitehat.com': {default: 'ignore'},
	}
}

tabManager.getTab.mockReturnValue( {
	tab: {
		id: 97
	},
	recordTracker: jest.fn()
} )

const interceptor = Interceptor( mockTrackerList )

describe( 'Interceptor', () => {
	it( 'should exist', () => {
		expect( interceptor ).toBeDefined()
	} )

	it( 'should expose a "handler" method', () => {
		expect( interceptor.handler ).toBeDefined()
	} )

	it( 'should block a "block" tracker found on the list', () => {
		const requestData = {
			id: 97,
			url: 'https://www.blackhat.com'
		}
		const result = interceptor.handler( requestData )
		expect( result && result.cancel ).toBeTruthy()
	} )

	it( 'should ignore an "ignore" tracker found on the list', () => {
		const requestData = {
			id: 97,
			url: 'https://www.whitehat.com'
		}
		const result = interceptor.handler( requestData )
		expect( result ).toBeUndefined()
	} )

	it( 'should not record a site that is not on the list', () => {
		const requestData = {
			id: 97,
			url: 'https://www.nothingtoseehere.com'
		}
		const result = interceptor.handler( requestData )
		expect( result ).toBeUndefined()
	} )

} )


