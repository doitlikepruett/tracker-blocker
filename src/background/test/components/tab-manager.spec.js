// import tabManager from '../../components/tab-manager'
import whitelistManager from '../../components/whitelist-manager'
whitelistManager.getWhitelist = jest.fn().mockReturnValue( {} )
let tabManager

const tabList = {463: {"id": 463, "status": "complete", "url": "https://www.google.com/", "trackersFound": {}, "hostname": "www.google.com", "whitelisted": false}, "464": {"id": 464, "status": "complete", "url": "https://www.cnn.com/", "trackersFound": {}, "hostname": "www.cnn.com", "whitelisted": false}}

describe( 'tabManager', () => {

	beforeEach( () => {
		tabManager = require( '../../components/tab-manager' ).default
		tabManager.createOrUpdateTab( tabList['463'] )
	} )

	it( 'should create a tab', () => {
		const tab = tabManager.getTab( 463 )
		expect( tab ).toBeDefined()
		expect( tab.id ).toEqual( 463 )
	} )

	it( 'should remove a tab', () => {
		expect( tabManager.getTab( 463 ) ).toBeDefined()
		tabManager.deleteTab( 463 )
		expect( tabManager.getTab( 463 ) ).not.toBeDefined()
	} )

} )