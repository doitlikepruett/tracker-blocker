import Tab from '../../components/tab.js'
import whitelistManager from '../../components/whitelist-manager'
jest.mock( '../../components/whitelist-manager' )

whitelistManager.getWhitelist.mockReturnValue( {'www.example.com': true} )

let tabData

beforeEach( () => {
	tabData = {url: 'https://www.test.com', id: 99, status: 'complete'}
} )

describe( 'Tab', () => {

	it( 'should exist', () => {
		expect( Tab ).toBeDefined()
	} )

	it( 'should create a tab', () => {
		const tab = Tab( tabData )
		expect( tab ).toBeDefined()
		expect( tab.id ).toEqual( 99 )
		expect( tab.url ).toEqual( 'https://www.test.com' )
		expect( tab.hostname ).toEqual( 'www.test.com' )
	} )


	it( 'should accruately set a whitelisted url on the tab', () => {
		const tab1 = Tab( tabData )
		expect( tab1.whitelisted ).toBeFalsy()

		tabData.url = 'https://www.example.com'
		const tab2 = Tab( tabData )
		expect( tab2.whitelisted ).toBeTruthy()
	} )

	it( 'should record trackers on the tracker list', () => {
		const tab = Tab( tabData )
		tab.recordTracker( {domain: 'cnn.com', action: 'blocked'} )
		tab.recordTracker( {domain: 'nytimes.com', action: 'ignored'} )
		expect( tab.trackersFound['cnn.com'] ).toEqual( 'blocked' )
		expect( tab.trackersFound['nytimes.com'] ).toEqual( 'ignored' )
	} )

	describe( 'update', () => {
		let tab

		beforeEach( () => {
			tab = Tab( tabData )
		} )

		it( 'should update the tab correctly', () => {
			tab.update( {id: 54, url: 'https://google.com', status: 'loading'} )
			expect( tab.id ).toEqual( 54 )
			expect( tab.url ).toEqual( 'https://google.com' )
			expect( tab.status ).toEqual( 'loading' )
		} )

		it( 'should reset the tab if the url changes', () => {
			tab.recordTracker( {domain: 'cnn.com', action: 'blocked'} )
			tab.update( {url: 'https://google.com'} )
			expect( Object.keys( tab.trackersFound ).length ).toEqual( 0 )
			expect( tab.hostname ).toEqual( 'google.com' )
		} )
	} )

} )